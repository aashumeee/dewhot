import 'package:flutter/material.dart';
import './pages/login.dart';
import './pages/registration.dart';
import './pages/forgotpassword.dart';
import './pages/loggedin/dashboard.dart';


void main() => runApp(AppGate());

class AppGate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dewhot app',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color.fromARGB(255, 0, 41, 65),
        accentColor: Colors.white,
        hintColor: Colors.white,
        buttonColor: Color.fromARGB(255, 231, 4, 19),
        fontFamily: 'RobotoCondensed',
        backgroundColor: Colors.black, 
        inputDecorationTheme: InputDecorationTheme(
          focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white)),
          enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white)),
          labelStyle: TextStyle(color: Colors.white, fontSize: 14.0, ),
          hintStyle: TextStyle(color: Colors.white30, fontSize: 13.0,),
        )
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => LoginPage(),
        '/register': (context) => RegistrationPage(),
        '/forgot-password': (context) => ForgotPasswordPage(),
        '/dashboard': (context) => DashboardPage(),
        // '/category-products': (context) => CategoryProducts(context),
      },
    );
  }
}
