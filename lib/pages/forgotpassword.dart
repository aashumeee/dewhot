import 'package:flutter/material.dart';

class ForgotPasswordPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/images/bg.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Container(
            padding: EdgeInsets.all(20.0),
            width: 350.0,
            height: 412.0,
            child: Column(children: <Widget>[
              Image.asset(
                'assets/images/logo.png',
                height: 100.0,
              ),
              Container(
                padding: EdgeInsets.all(20.0),
                child: Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: ForgotPasswordForm(),
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}

class ForgotPasswordForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          style: (TextStyle(
            color: Colors.white,
          )),
          decoration: new InputDecoration(
            hintText: 'Write your email',
            labelText: 'EMAIL',
            prefixIcon: const Icon(
              Icons.email,
              color: Colors.white,
              size: 18.0,
            ),
          ),
        ),
        SizedBox(
          height: 20.0,
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: RaisedButton.icon(
                onPressed: () => {},
                label: Text(
                  'RESET PASSWORD',
                  style: TextStyle(color: Colors.white),
                ),
                icon: const Icon(
                  Icons.arrow_right,
                  size: 30.0,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 40.0,
        ),
        GestureDetector(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.arrow_left, color: Colors.white,),
              Text('Back', style: TextStyle(color:Colors.white),),
            ],
          ),
          onTap: () {
            Navigator.pushNamed(context, '/');
          },
        ),
      ],
    );
  }
}
