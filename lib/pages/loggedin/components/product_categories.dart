import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../cat_product_list.dart';

class ProductCategories extends StatefulWidget {
  @override
  _ProductCategoriesState createState() => _ProductCategoriesState();
}

class _ProductCategoriesState extends State<ProductCategories> with AutomaticKeepAliveClientMixin<ProductCategories>{

  final String apiUrl = 'https://gasgeysers.co.za/dealerportal/wp-json/api/product-categories';
  List productCategories = [];

  //
  // Trigger api async
  //
  Future<String> getProductCategoriesList() async{
    var res = await http.get(Uri.encodeFull(apiUrl), headers: { "Accept": "applications/json"});
    print('data downloaded');
    setState(() {
      var resBody = json.decode(res.body);
      productCategories = resBody['payload'];
    });
    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
        child: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            expandedHeight: 120.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text('PRODUCT CATEGORIES', style: TextStyle(fontSize: 14.0),),
              background: Image.network(
                "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
                fit: BoxFit.cover,
              ),
            ),
            automaticallyImplyLeading: false,
          ),

          SliverPadding(
            padding: EdgeInsets.all(15.0),
            sliver: SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
              childAspectRatio: 1.45,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CategoryProducts(categoryID : productCategories[index]['id'], categoryTitle : productCategories[index]['title'])),
                      );
                    },
                    child:Container(
                      padding: EdgeInsets.all(15.0),
                      alignment: Alignment.center,
                      color: Color.fromRGBO(243, 243, 243, 1),
                      child: Column(
                        children: <Widget>[
                          Image.network(productCategories[index]["thumbnail"], height: 70.0,),
                          SizedBox(height:10.0),
                          Text(productCategories[index]["title"].toUpperCase(), style: TextStyle(color:Colors.black),),
                        ],
                      ),
                  ),
                );
              },
              childCount: productCategories.length,
              addAutomaticKeepAlives: wantKeepAlive,
              semanticIndexOffset: 1,
            ),
          ),
          ),
      
        ],
      )
    );
  }

  @override
  void initState() {
    super.initState();
    this.getProductCategoriesList();
  }

  @override
  bool get wantKeepAlive => true;
}