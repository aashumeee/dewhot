import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//
// Includes 
//
import 'inc/left_drawer.dart';
import 'inc/top_app_bar.dart';
import 'inc/bottom_navigation.dart';
import 'components/product_categories.dart';
import 'components/events.dart';
import 'components/technical.dart';
import 'components/support.dart';
import 'components/forum.dart';


class DashboardPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          appBar: TopAppBar(),
          drawer: LeftDrawer(),
          bottomNavigationBar: BottomNavigation(),
          body: DefaultTabController(
          length: 5,
          child: Column(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.only(top:10.0),
                  color: Color.fromARGB(255, 231, 4, 19),
                  child: TabBar(
                  indicatorColor: Colors.red,
                  labelStyle: TextStyle(color: Colors.black),
                  indicatorWeight: 0.1,
                  tabs: [
                    Tab( child: Column(
                        children: <Widget>[
                          Icon(FontAwesomeIcons.superpowers,size: 20.0,),
                          Text('Category', style: TextStyle(fontSize: 12.0, fontFamily: 'RobotoCondensed', fontWeight: FontWeight.w400),)
                        ],
                      ),
                    ),
                    Tab( child: Column(
                        children: <Widget>[
                          Icon(Icons.local_dining ,size: 20.0,),
                          Text('Events', style: TextStyle(fontSize: 12.0, fontFamily: 'RobotoCondensed', fontWeight: FontWeight.w400,),)
                        ],
                      ),
                    ),
                    Tab( child: Column(
                        children: <Widget>[
                          Icon(Icons.multiline_chart,size: 20.0,),
                          Text('Technical', style: TextStyle(fontSize: 12.0, fontFamily: 'RobotoCondensed', fontWeight: FontWeight.w400),)
                        ],
                      ),
                    ),
                    Tab( child: Column(
                        children: <Widget>[
                          Icon(Icons.tune,size: 20.0,),
                          Text('Support', style: TextStyle(fontSize: 12.0, fontFamily: 'RobotoCondensed', fontWeight: FontWeight.w400),)
                        ],
                      ),
                    ),
                    Tab( child: Column(
                        children: <Widget>[
                          Icon(Icons.forum,size: 20.0,),
                          Text('Forum', style: TextStyle(fontSize: 12.0, fontFamily: 'RobotoCondensed', fontWeight: FontWeight.w400),)
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child:  TabBarView(
                  children: [
                  ProductCategories(),
                  Events(),
                  Technical(),
                  Support(),
                  Forum(),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
  }
}