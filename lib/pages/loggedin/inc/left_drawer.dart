import 'package:flutter/material.dart';

class LeftDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Color.fromARGB(255, 0, 41, 65),
        child: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            UserAccountsDrawerHeader(
              margin: EdgeInsets.only(bottom: 0.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/images/account-bg.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
              accountName: Text("Brigitte Dewberry"),
              accountEmail: Text("info@pixelanddigit.com"),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.brown.shade800,
                radius: 20,
                child: Container(
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage('assets/images/brigitte.png'),
                    ),
                  ),
                ),
              ),
            ),
     
            Divider(height: 1.0, color: Colors.white),
            ListTile(
              leading: const Icon(
                Icons.exit_to_app,
                color: Colors.red,
              ),
              title: const Text(
                'LOGOUT',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.pushNamed(context, '/');
              }),
          ],
        ),
      ),
    );
  }
}
