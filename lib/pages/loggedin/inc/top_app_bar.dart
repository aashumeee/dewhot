import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class TopAppBar extends StatelessWidget implements PreferredSizeWidget {
  Size get preferredSize {
    return new Size.fromHeight(50.0);
  }
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: const Text('DEWHOT', style:(TextStyle( fontSize: 14.0))),
      centerTitle: false,
      actions: [
        IconButton(
          alignment: Alignment.centerRight,
          padding:  EdgeInsets.only(top:0 , right: 0, bottom: 0 , left: 0),
          iconSize: 18.0,
          icon: Icon(Icons.phone),
          onPressed: _phoneLuncher,
        ),
        IconButton(
          alignment: Alignment.centerRight,
          iconSize: 18.0,
          icon: Icon(Icons.email),
          onPressed: _emailLuncher,
        ),
      ],
    );
  }
}

_phoneLuncher() async {
  if (await canLaunch('sms:5550101234')) {
    await launch('sms:5550101234');
  } else {
    print('error lunch url');
  }
}

_emailLuncher() async {
 // const url = 'tel:9849039761';
  if (await canLaunch('mailto:smith@example.org?subject=News&body=New%20plugin')) {
    await launch('mailto:smith@example.org?subject=News&body=New%20plugin');
  } else {
    print('error lunch url');
  }
}