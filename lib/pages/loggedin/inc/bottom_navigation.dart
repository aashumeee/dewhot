import 'package:flutter/material.dart';

class BottomNavigation extends StatefulWidget implements PreferredSizeWidget{

  Size get preferredSize {
    return new Size.fromHeight(50.0);
  }

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {

    return   new Theme(
    data: Theme.of(context).copyWith(
        // sets the background color of the `BottomNavigationBar`
        canvasColor: Color.fromARGB(255, 0, 41, 65),
        // sets the active color of the `BottomNavigationBar` if `Brightness` is light
        primaryColor: Colors.red,
        textTheme: Theme.of(context).textTheme.copyWith(caption: new TextStyle(color: Colors.white))), // sets the inactive color of the `BottomNavigationBar`
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: 0,
          fixedColor: Colors.red,
          items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
          icon: Icon(Icons.category), title: Text("Shop"),),
          BottomNavigationBarItem(icon: Icon(Icons.favorite_border), title: Text("Favorites"), ),
          BottomNavigationBarItem(icon: Icon(Icons.notifications), title: Text("Notifications")),
          BottomNavigationBarItem(icon: Icon(Icons.add_shopping_cart), title: Text("Cart")),
          BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text("Settings")),
        ],
        ),
      );

    return  BottomNavigationBar(
      

      onTap: (value) {
        print(value);
        // final routes = ["/list", "/map"];
        // _currentIndex = value;
        // Navigator.of(context).pushNamedAndRemoveUntil(
        //     routes[value], (route) => false);
      },
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
        icon: Icon(Icons.category), title: Text("Shop"),),
        BottomNavigationBarItem(icon: Icon(Icons.favorite_border), title: Text("Favorites"), ),
        BottomNavigationBarItem(icon: Icon(Icons.notifications), title: Text("Notifications")),
        BottomNavigationBarItem(icon: Icon(Icons.location_on), title: Text("Near me")),
        BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text("Settings")),
      ],
      currentIndex: 0,
      type: BottomNavigationBarType.fixed,
      fixedColor: Colors.red,

    );
  }
}
