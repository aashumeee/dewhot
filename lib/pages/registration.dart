import 'package:flutter/material.dart';
import 'package:validate/validate.dart';

class RegistrationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Container(
            padding: EdgeInsets.all(20.0),
            width: 350.0,
            height: 654.0,
            child: Column(children: <Widget>[
              Image.asset(
                'assets/images/logo.png',
                height: 100.0,
              ),
              Container(
                padding: EdgeInsets.all(20.0),
                child: Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: RegistrationForm(),
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}

class RegistrationForm extends StatefulWidget {
  @override
  _RegistrationFormState createState() => _RegistrationFormState();
}

class _RegistrationFormState extends State<RegistrationForm> {
  final _formKey = GlobalKey<FormState>();
  final registerController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    registerController.dispose();
    super.dispose();
  }

  String _fullName;
  String _email;
  String _phone;
  String _username;
  String _password;
  bool _validate = false;

  //
  // Validations
  //
  String _validateRequired(String value) {
    if(value.isEmpty){
      return 'Empty field.';
    }
    return null;
  }
  String _validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'Invalid Email Address.';
    }
    return null;
  }

  String _validatePassword(String value) {
    if (value.length < 8) {
      return 'The Password must be at least 8 characters.';
    }
    return null;
  }

  String _validatePhone(String value) {
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  //
  // Action
  //
  void register() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      final snackBar = SnackBar(
        backgroundColor: Color.fromARGB(255, 231, 4, 19),
        content: Text("Your account successfully created. Please login.", style: TextStyle(color: Colors.white),),
      );
      Scaffold.of(context).showSnackBar(snackBar);
      Future.delayed(const Duration(milliseconds: 2000), () {
        // Navigator.push(
        //   context,
        //   MaterialPageRoute(builder: (context) => LoginScreen()),
        // );
      });
    } else {
      setState(() {
        _validate = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      autovalidate: _validate,
      child: Column(
        children: [
          TextFormField(
            validator: _validateRequired,
            onSaved: (String value) {
              setState(() {
                _fullName = value;
              });
            },
            style: (TextStyle(color: Colors.white,)),
            decoration: InputDecoration(
              hintText: 'Write your full Name',
              labelText: 'FULL NAME',
              prefixIcon: const Icon(
                Icons.account_circle,
                color: Colors.white,
                size: 18.0,
              ),
              errorStyle: TextStyle(fontSize: 12.0, color: Colors.white)
            ),
          ),
          TextFormField(
            validator: _validateEmail,
            onSaved: (String value) {
              setState(() {
                _email = value;
              });
            },
            keyboardType: TextInputType.emailAddress,
            style: (TextStyle(
              color: Colors.white,
            )),
            decoration: InputDecoration(
              hintText: 'Write your email',
              labelText: 'EMAIL',
              prefixIcon: const Icon(
                Icons.email,
                color: Colors.white,
                size: 18.0,
              ),
              errorStyle: TextStyle(fontSize: 12.0, color: Colors.white)
            ),
          ),
          TextFormField(
            validator: _validatePhone,
            onSaved: (String value) {
              setState(() {
                _phone = value;
              });
            },
            keyboardType: TextInputType.phone,
            style: (TextStyle(
              color: Colors.white,
            )),
            decoration: InputDecoration(
              hintText: 'Write your phone number',
              labelText: 'PHONE',
              prefixIcon: const Icon(
                Icons.phone,
                color: Colors.white,
                size: 18.0,
              ),
              errorStyle: TextStyle(fontSize: 12.0, color: Colors.white)
            ),
          ),
          TextFormField(
            validator: _validateRequired,
            onSaved: (String value) {
              setState(() {
                _username = value;
              });
            },
            style: (TextStyle(
              color: Colors.white,
            )),
            decoration: InputDecoration(
              hintText: 'Write your username',
              labelText: 'USERNAME',
              prefixIcon: const Icon(
                Icons.person,
                color: Colors.white,
                size: 18.0,
              ),
              errorStyle: TextStyle(fontSize: 12.0, color: Colors.white)
            ),
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            validator: _validatePassword,
            onSaved: (String value) {
              setState(() {
                _password = value;
              });
            },
            obscureText: true,
            style: (TextStyle(
              color: Colors.white,
            )),
            decoration: InputDecoration(
              hintText: 'Write your password',
              labelText: 'PASSWORD',
              prefixIcon: const Icon(
                Icons.lock,
                color: Colors.white,
                size: 18.0,
              ),
              errorStyle: TextStyle(fontSize: 12.0, color: Colors.white)
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  onPressed: this.register,
                  color: Colors.red,
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center, // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Text(
                        "REGISTER",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      Icon(
                        Icons.arrow_right,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          GestureDetector(
            
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.arrow_left,
                  color: Colors.white,
                ),
                Text(
                  'Back',
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
          ),
        ],
      ),
    );
  }
}
