import 'package:flutter/material.dart';
import 'package:validate/validate.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 0, 41, 65),
      body: Center(
        child: Container(
          margin: EdgeInsets.only(top: 120.0),
          padding: EdgeInsets.all(20.0),
          width: 350.0,
          // color:Color.fromARGB(255, 255, 255, 255),
          child: Column(children: <Widget>[
            Image.asset(
              'assets/images/logo.png',
              height: 100.0,
            ),
            Container(
              padding: EdgeInsets.all(20.0),
              child: Container(
                margin: EdgeInsets.only(top: 20.0),
                child: LoginForm(),
              ),
            ),
          ]),
        ),
      ));
    }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  
  final _formKey = GlobalKey<FormState>();
  final loginController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    loginController.dispose();
    super.dispose();
  }
  String _email;
  String _password;
  bool _validate = false;

  // Validations
  String _validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'Invalid Email Address.';
    }
    return null;
  }
  String _validatePassword(String value) {
    if (value.length < 8) {
      return 'The Password must be at least 8 characters.';
    }
    return null;
  }

  // Actions
  void login() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Navigator.pushNamed(context, '/dashboard');
    } else {
      setState(() {
        _validate = true;
      });
    }
  }

  // Build
  @override
  Widget build(BuildContext context) {
    return  Form(
      key: _formKey,
      autovalidate: _validate,
      child: Column(
      children: [
        TextFormField(
          keyboardType: TextInputType.emailAddress,
          initialValue: 'asdasd@asdasd.asdasd',
          onSaved: (String value) {
            setState(() {
              _email = value;
            });
          },
          style: (TextStyle(
            color: Colors.white,
          )),
          decoration: InputDecoration(
            hintText: 'Write your username',
            labelText: 'USERNAME',
            prefixIcon: const Icon(
              Icons.person,
              color: Colors.white,
              size: 18.0,
            ),
            errorStyle: TextStyle(fontSize: 12.0, color: Colors.white)
          ),
          validator: _validateEmail,
        ),
        SizedBox(
          height: 20,
        ),
        TextFormField(
          onSaved: (String value) {
            setState(() {
              _password = value;
            });
          },
          obscureText: true,
          initialValue: 'asdasdasdasd',
          style: (TextStyle(
            color: Colors.white,
          )),
          decoration: new InputDecoration(
            hintText: 'Write your password',
            labelText: 'PASSWORD',
            prefixIcon: const Icon(
              Icons.lock,
              color: Colors.white,
              size: 18.0,
            ),
           errorStyle: TextStyle(fontSize: 12.0, color: Colors.white)
          ),
          validator: _validatePassword,
        ),
        SizedBox(
          height: 20.0,
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: RaisedButton(
                onPressed: this.login,
                color: Color.fromARGB(255, 231, 4, 19),
                child: Text(
                  'LOGIN', 
                  style: TextStyle(
                    color: Colors.white, 
                    fontSize: 16.0,
                    fontFamily: 'RobotoCondensed',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
        GestureDetector(
           child: Padding(
             padding: EdgeInsets.only(top: 10.0, bottom: 15.0 ),
             child: Text('Forgot your password?',  style: TextStyle(color: Colors.grey.shade500),)
           ),
           onTap: () {
            Navigator.pushNamed(context, '/forgot-password');
          },
        ),
         GestureDetector(
           child: Text('Register', style: TextStyle(color: Colors.white),),
           onTap: () {
            Navigator.pushNamed(context, '/register');
          },
        ),
      ],
    )
    );
  }
}